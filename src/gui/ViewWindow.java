package gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class ViewWindow extends JFrame {
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ViewWindow vw = new ViewWindow();
				vw.createAndShowGui();				
			}
		});
		return;
	}
	
	/**
	 * Debug information
	 */
	static int mouseX, mouseY;
	/**
	 * Debug information
	 */
	static float mouseDragY, mouseDragMod = 10;
	
	/**
	 * Used to control main window size
	 */
	static Dimension windowPrefSize = new Dimension();
	/**
	 * Used to control scroll view size
	 */
	static Dimension scrollWindowPrefSize = new Dimension();
	
	/**
	 * The folder the user desires to sort
	 */
	File sortingFolder;
	/**
	 * A FILE with path set to the current image being displayed
	 */
	File imageCursor;
	/**
	 * An instance of the Tool class used to control almost all functionality in the program
	 */
	static Tool myFolderTools = new Tool();
	
	/**
	 * All setup is done in createAndShowGui()
	 */
	public ViewWindow() {}
	
	/**
	 * 
	 */
	private void createAndShowGui() {
		// The program runs! Yay!
		// System.out.println("Hello World!");
		
		// Set some defaults for the window sizes to make sure everything has enough room and looks good
		windowPrefSize = new Dimension(500,500);
		scrollWindowPrefSize = new Dimension(450,500);
		
		/**
		 * The content pane for the JFrame
		 */
		Container contentPane = this.getContentPane();
		
		/**
		 * Create a panel to put inside the scrollView for easier control<br>
		 * scrollView<br>
		 * 	|- scrollViewPanel<br>
		 * 		|- stuff<br>
		 */
		JPanel scrollViewPanel = new JPanel();
		
		/**
		 * Create the scroll pane that will house the folder buttons 
		 */
		JScrollPane scrollView = new JScrollPane(scrollViewPanel);
		// Always show the vertical scroll bar
		scrollView.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		// Control the size
		scrollView.setPreferredSize(windowPrefSize);
		// Add it to the main window
		contentPane.add(scrollView);
		
		/**
		 * Create a new container to store all the folder information<br>
		 * Using a container so exact positioning of buttons can be controlled<br>
		 * Owned by scrollViewPanel
		 */
		Container stuff = new Container();

		// Put all the folder stuff in the scroll view panel
		scrollViewPanel.add(stuff);

		// Make sure to define the size of the container so it looks right
		stuff.setPreferredSize(scrollWindowPrefSize);

		/**
		 * Viewing panel for current image
		 */
		JPanel imageView = new JPanel();
		/**
		 * Create a contaienr and add that to the image view for more direct image location control
		 */
		Container imageCont = new Container();
		imageView.add(imageCont);
		/**
		 * The label that will be used to display the image currently being previewed
		 */
		JLabel imageLabel = new JLabel();
		// Give the label a bounding box and then put it in the container
		imageLabel.setBounds(0,0,500,500);
		imageCont.add(imageLabel);
		
		// Control the size of the image preview sections
		imageView.setPreferredSize(windowPrefSize);
		imageCont.setPreferredSize(windowPrefSize);
		// Add the image panel to the left side of the window
		contentPane.add(imageView,BorderLayout.WEST);
		
		// Hand references to components down to the functionality tool for ease of use
		myFolderTools.giveRef(stuff,scrollViewPanel, this);
		myFolderTools.setImagePointers(imageCont,imageLabel);

		// Initialize the tool once
		myFolderTools.initialize();
		// Run setup for the first time to get everything in place
		myFolderTools.setup();
		
		// Was used for debuging, not needed for release
//		/**
//		 * Adds mouse motion listener to more easily find coordinates for placing objects on page
//		 */
//		MouseMotionListener mmListener = new MouseMotionListener() {
//			/**
//			 * Adds basic drag controls to the scroll view
//			 */
//			public void mouseDragged(MouseEvent arg0) {
//				JScrollBar vertical = scrollView.getVerticalScrollBar();
//				mouseDragY += (mouseY-arg0.getY())/mouseDragMod;
//				if(mouseDragY != 0) {
//					vertical.setValue(vertical.getValue() + (int)mouseDragY);
//					mouseDragY = 0;
//				}
//			}
//			public void mouseMoved(MouseEvent arg0) {
//				mouseX = arg0.getX();
//				mouseY = arg0.getY();
//				//System.out.println("Mouse x: " + mouseX + " Mouse y: " + mouseY);
//			}
//		};
//		scrollViewPanel.addMouseMotionListener(mmListener);
		
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}