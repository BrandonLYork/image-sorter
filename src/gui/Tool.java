package gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;


public class Tool {
	/** 
	 * Vector list of all formats considered valid<br>
	 *  getImages will skip any file types not in Formats
	 */
	Vector<String> Formats;
	
	/**
	 * List of all the images found in the sortingFolder
	 */
	Vector<String> images = new Vector<String>();
	
	/**
	 * A vector for all sub folders spawned
	 */
	Vector<JFrame> myChildren = new Vector<JFrame>();
	/**
	 * A reference to the base tool for recursive iterations to reference
	 */
	Tool myRef = null;
	/**
	 * Folder that is currently being sorted
	 */
	File sortingFolder;
	/**
	 * Set when the control buttons have been created and added to the main preview window
	 */
	boolean controlsCreated = false;
	/**
	 * Opens a file explorer window to select a new folder to sort
	 */
	JButton browseFolder;
	/**
	 * Text field to input a direct path, will be auto-populated by the output from the browseFolder button file explorer 
	 */
	JTextField folderInput;
	/**
	 * Closes all subfolder windows and pulls the current sortingFolder to display an updated list of sub folders
	 */
	JButton refreshFolders;
	/**
	 * Iterates the image view JLabel to the next image in the list
	 */
	JButton nextImage;
	/**
	 * Iterates the image view JLabel to the previous image in the list
	 */
	JButton previousImage;
	// Handle the current image
	/**
	 * An absolute path to the current image being previewed
	 */
	String currentImg;
	/**
	 * Used while moving through the image list 
	 */
	int imageCursor;
	/**
	 * A path to the image with the image name stripped off
	 */
	String imagePath;
	/**
	 * The full name of the image including file type
	 */
	String imageName;

	// Pointers to certain jpanels
	// TODO : Think of better way to access jpanels instead of pointers
	/**
	 * Pointer to the main viewing window @JFrame
	 */
	ViewWindow viewHold;
	/**
	 * Pointer to the scroll view for the folder buttons in the main view window
	 */
	JPanel scrollHold;
	/**
	 * Pointer to the container inside the scrollview in the main view window<br>
	 * Actually holds all the buttons
	 */
	Container stuffHold;
	
	/**
	 * Pointer to the container that owns JLabel used to preview the current image
	 */
	Container imageVHold;
	/**
	 * Pointer to the label displaying the current image
	 */
	JLabel imageLHold;
	/**
	 * Label used to display some basic information to the user to understand amount of images to sort and current location in list of accepted images
	 */
	JLabel imagePlace;
	
	Tool() {
		// Default file types if no proper format file selected
		Formats = new Vector<String>();//{".png",".jpg",".gif"};
		imagePlace = new JLabel();
		imagePlace.setBounds(10,80,300,20);
		folderInput = new JTextField();
	}
	
	/**
	 *  Reads the select format file and inserts acceptable file format types into list
	 */
	void readFormats() {
		File allFormats;
		JFrame folderView = new JFrame();
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Choose Format file");
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
			    "Format File", "fmt", "txt");
		fileChooser.setFileFilter(filter);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int retVal = fileChooser.showSaveDialog(folderView);
		if(retVal == JFileChooser.APPROVE_OPTION) {
			// return fileChooser.getSelectedFile();
			allFormats = fileChooser.getSelectedFile();
		} else {
			allFormats = null;
		}
		
		if(allFormats == null) {
			Formats.add("png");
			Formats.add("jpg");
			Formats.add("gif");
		} else {
			try {
				BufferedReader br = new BufferedReader(new FileReader(allFormats));
				String st;
				while ((st = br.readLine()) != null) {
					Formats.add(st);
				}
				br.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	
	/**
	 *  Simple versions with default values
	 * @param path - Path of where to find sub folders
	 * @return Amount of folders found
	 */
	public int folderContent(File path) {
		return folderContent(stuffHold,path,0);
	}
	/**
	 *  Simple versions with default values
	 * @param path - Path of where to find sub folders
	 * @param holder - A reference to the frame that is going to hold the buttons being created
	 * @return Amount of folders found
	 */
	public int folderContent(Container holder,File path) {
		return folderContent(holder,path,0);
	}
	/**
	 * Generates a list of folder buttons for sub folders
	 * @param Holder - The frame that is going to hold the new content being generated
	 * @param path - The folder path where information is being gathered from
	 * @param Offset - An offset in case more tool buttons need to be added above the list of sub-folders
	 * @return dirList.length - Returns the amount of folders found in the directory
	 */
	public int folderContent(Container Holder,File path,int Offset) {
		if(Holder == null || !path.exists()) {
			System.out.println("Folder / Structure invalid");			
			return -1;
		}
		File[] dirList = path.listFiles(File::isDirectory);
		if(dirList == null) return 0;
		for(int i = 0;i < dirList.length;i++) {
			System.out.println(dirList[i].toString());
			Holder.add(createFolderButton(dirList[i].toString(), 10, Offset + (i * 30)));
		}
		return dirList.length;
	}
	
	/**
	 * Generates a single button for a sub folder that will open a new frame is pressed and generate it's own sub folder buttons
	 * @param Path - Path to the sub folder
	 * @param x - Relative x position in frame
	 * @param y - Relative y position in frame
	 * @return jb - Returns a reference to the newly created button
	 */
	private JButton createFolderButton(String Path, int x, int y) {
		if(x == -1) x = 0;
		if(y == -1) y = 0;
		
		/** 
		 * Had to replace  backslash with ! to avoid issues with split function
		 */
		String[] pathSplit = Path.replace('\\', '!').split("!");
		double mod = 5.6;
		double len = pathSplit[pathSplit.length-1].length();
		if(len < 6) mod = 7;
		int w = (int)(40 + (double)(pathSplit[pathSplit.length-1].length() * mod));

		JButton jb = new JButton(pathSplit[pathSplit.length-1]);
		
		jb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				CreateFolderWindow(Path);
			}
		
		});
		jb.setBounds(x, y, w, 30);
		return jb;
	}
	
	/**
	 * Creates a button to copy the current image to the relevant path
	 * @param path - The path that the picture should be saved to
	 * @param x - Relative x position
	 * @param y - Relative y position
	 * @return jb - Returns a reference to the newly created button
	 */
	public JButton createSaveButton(String path, int x, int y) {
		if(x == -1) x = 0;
		if(y == -1) y = 0;		
		
		int w = (int)(95);

		JButton jb = new JButton("Save Here");
		
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getImageInfo(currentImg);
				InputStream is = null;
				OutputStream os = null;
				try {
					is = new FileInputStream(imagePath + imageName);
					os = new FileOutputStream(path + "\\" + imageName);
					byte [] buffer = new byte[1024];
					int length;
					while((length = is.read(buffer)) > 0) {
						os.write(buffer, 0, length);
					}
					
					// TODO : remove file after confirming that it copied
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						is.close();
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		
		});
		jb.setBounds(x, y, w, 30);
		return jb;
	}
	
	/**
	 * Creates a button to cut the current image and paste it in the relevant path<br>
	 *  Iterates the main view windows image and removes the current image from the list
	 * @param path - The path that the picture should be saved to
	 * @param x - Relative x position
	 * @param y - Relative y position
	 * @return jb - Returns a reference to the newly created button
	 */
	public JButton createCutButton(String path, int x, int y) {
		if(x == -1) x = 0;
		if(y == -1) y = 0;		
		
		int w = (int)(105);
								  
		JButton jb = new JButton("Cut and Save");
		
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getImageInfo(myRef.currentImg);
				InputStream is = null;
				OutputStream os = null;
				try {
					is = new FileInputStream(myRef.imagePath + myRef.imageName);
					os = new FileOutputStream(path + "\\" + myRef.imageName);
					byte [] buffer = new byte[1024];
					int length;
					while((length = is.read(buffer)) > 0) {
						os.write(buffer, 0, length);
					}
					
					// TODO : remove file after confirming that it copied
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						is.close();
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					File toDelete = new File(myRef.imagePath + myRef.imageName);
					if(!toDelete.delete()) System.out.println("Could not delete file");
					else {
						myRef.images.remove(myRef.imageCursor);
						myRef.imageCursor = myRef.imageCursor - 1;
						if(myRef.imageCursor<0) myRef.imageCursor = 0;
						myRef.setImage(myRef.imageCursor);
						
						
						myRef.getImageInfo(myRef.currentImg);
					}
				}
			}
		
		});
		jb.setBounds(x, y, w, 30);
		return jb;
	}
	
	/**
	 * Create child sub window with buttons for each subsequent sub folder
	 * @param Path : Path that folder window is related to
	 * @return
	 */
	private int CreateFolderWindow(String Path) {
		Tool ref = myRef;
		javax.swing.SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
            	/** 
            	 * Used to create sub children folder windows
            	 */
            	Tool tempTool = new Tool();
                JFrame frame = new JFrame(Path);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                
                //tempTool.currentImg = ref.currentImg;
                tempTool.myRef = ref;
                
                JPanel panel = new JPanel();
                panel.setOpaque(true);
                
                Container cont = new Container();
                
                JScrollPane scrollView = new JScrollPane(cont);
        		scrollView.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        		scrollView.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                
                panel.add(scrollView);
                
                //cont.add(tempTool.createSaveButton(Path,currentImg, 10, 10));
                cont.add(tempTool.createSaveButton(Path, 10, 10));
                cont.add(tempTool.createCutButton(Path, 105, 10));
                
                int len = tempTool.folderContent(cont, new File(Path), 50);
                //           (folders ) + Create Folder + Save Button
                int height = (len * 30) + 50            + 0;
                
                Dimension dim = new Dimension(300,height + 10);
                cont.setPreferredSize(dim);
                scrollView.validate();
                
                if(height > 500) height = 500;
                Dimension dim2 = new Dimension(300, height + 10);
                scrollView.setPreferredSize(dim2);
                
                
                frame.getContentPane().add(BorderLayout.CENTER, panel);
                frame.setLocationByPlatform(true);
                frame.pack();
                frame.setVisible(true);
                frame.setResizable(true);
                myChildren.add(frame);
            }
        });
		return 0;
	}
	
	/**
	 * Closes all child folder windows
	 * @return 0 - Success
	 */
	public int closeAllFolders() {
		while(myChildren.size() > 0) {
			myChildren.get(0).dispose();
			myChildren.remove(0);
		}		
		return 0;
	}

	/**
	 * Opens a file explorer dialogue box<br>
	 * 	Used to select the folder with images that needs to be sorted
	 * @return Returns the file selected
	 */
	public File openBrowse() {
		JFrame folderView = new JFrame();
		folderView.setTitle("Choose Folder to Sort");
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Choose Folder to Sort");
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int retVal = fileChooser.showSaveDialog(folderView);
		if(retVal == JFileChooser.APPROVE_OPTION) {
			return fileChooser.getSelectedFile();
		}
		return null;
	}


	/**
	 * sortingFolder Setter
	 * @param sf
	 */
	public void setFolder(File sf) {
		sortingFolder = sf;
	}

	/**
	 * Creates the control buttons<br>
	 * 	* browseFolder<br>
	 * 	* nextImage<br>
	 *  * previousImage<br>
	 *  * refreshFolders<br>
	 *  * folderInput<br>
	 */
	public void createControls() {
		if(controlsCreated) return;
		controlsCreated = true;
		/**
		 * Button to browse for folder
		 */
		// Button to browse
		browseFolder = new JButton("Browse");
		browseFolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File f = openBrowse();
				folderInput.setText(f.getAbsolutePath());
				sortingFolder = f;
			}
		});
		browseFolder.setBounds(350, 0, 80, 30);

		//folderInput = new JTextField(sortingFolder.getAbsolutePath());
		folderInput.setEditable(true);
		
		folderInput.setBounds(10, 1, 340, 30);
		
		/**
		 * Add a button that polls the target folder to check for any updates
		 * Closes all current sub folder windows
		 */
		refreshFolders = new JButton("Refresh");		
		refreshFolders.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File F = new File(folderInput.getText());
				if(F != null) sortingFolder = F;
				closeAllFolders();
				clearOldContent();
				addControls();
				//int length = folderContent(stuffHold,sortingFolder,100);
				int length = setup();
				stuffHold.setPreferredSize(new Dimension(450,(length * 30) + 100));
				scrollHold.setPreferredSize(new Dimension(450,(length * 30) + 100));
				viewHold.validate();
			}
		});		
		refreshFolders.setBounds(10, 30, 100, 20);

		/**
		 * Button to navigate to the next image
		 */
		nextImage = new JButton("Next Image");
		nextImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(imageCursor+1>=images.size()) imageCursor = 0;
				else imageCursor++;
				setImage(imageCursor);
			}
		});
		nextImage.setBounds(10, 50, 120, 30);
		
		/**
		 * Button to navigate to the previous image
		 */
		previousImage = new JButton("Previous Image");
		previousImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(imageCursor-1<0) imageCursor = images.size()-1;
				else imageCursor--;
				setImage(imageCursor);
			}
		});
		previousImage.setBounds(130, 50, 130, 30);
	}
	
	/**
	 * Creates the control components and then adds them all the main preview window
	 */
	public void addControls() {
		//stuffHold = Stuff;
		createControls();
		stuffHold.add(browseFolder);
		stuffHold.add(folderInput);
		stuffHold.add(refreshFolders);
		stuffHold.add(nextImage);
		stuffHold.add(previousImage);
		stuffHold.add(imagePlace);
	}
	
	/**
	 * Removes all current content in preperation for refreshing
	 */
	public void clearOldContent() {
		stuffHold.removeAll();
		stuffHold.repaint();
		stuffHold.validate();
		
		scrollHold.repaint();
		scrollHold.validate();
	}
	
	/**
	 * Retrieves all the valid image files found in the sortingFolder
	 */
	void getImages() {
		images.clear();
		File[] imgList = sortingFolder.listFiles(File::isFile);
		String[] strTempL;
		String[] strTemp;
		for(int i = 0;i < imgList.length;i++) {
			System.out.println(imgList[i].toString());
			strTempL = imgList[i].toString().replace('\\', '!').split("!");
			strTemp = strTempL[strTempL.length-1].replace('.', '!').split("!");
			if(strTemp.length < 2) {}
			else if(Formats.contains(strTemp[strTemp.length-1])) {
				images.add(imgList[i].toString());
			}
			else {
				System.out.println("Failed to load" + strTemp);
			}
		}
	}
	
	/**
	 * Updates the image label in the main preview window with the current image being viewed<br>
	 * 	Made generic to add error handling<br>
	 * 	Makes a function call to setImage(int cursor) with default value of 0
	 */
	void setImage() {
		setImage(0);
	}
	/**
	 * Updates the image label in the main preview window with the current image being viewed<br>
	 * 	This makes a call to setImage(String Path)<br>
	 * 	Function simplifies call to only needing integer location
	 * @param cursor The location of the current desired image in the image list
	 */
	void setImage(int cursor) {
		if(images.isEmpty()) {
			imageLHold.setText("No images found");
			imageLHold.setIcon(null);
		}
		else setImage(images.get(cursor));
	}
	/**
	 * Updates the image label in the main preview window with the current image being viewed
	 * @param Path - The absolute path to the current desired image to preview
	 */
	void setImage(String Path) {
		
		imagePlace.setText("Current Image Viewing : " + Integer.toString(imageCursor+1) + " Images Left : " + Integer.toString(images.size()));
		ImageIcon icon = createImageIcon(Path,"Picture");
		if(icon == null) imageLHold.setText("Icon creation failed");
		else {
			imageLHold.setIcon(icon);
			//imageLHold.setText(Integer.toString(imageCursor));
		}
		imageVHold.validate();
		currentImg = Path;
		
		getImageInfo(currentImg);
		viewHold.setTitle(imageName);
	}
	
	/**
	 * Extracts the image path and the image name and saves the data to the related class values
	 * @param img The absolute path to the image information requested
	 */
	void getImageInfo(String img) {
		/** 
		 * Had to replace  backslash with ! to avoid issues with split function
		 */
		if(img == null) return;
		String [] hold = img.replace('\\','!').split("!");
		imageName = hold[hold.length-1];
		imagePath = "";
		for(int i = hold.length-2;i >= 0;i--) {
			imagePath = hold[i] + "\\" + imagePath;
		}
		System.out.println("Done");
	}
	
	/**
	 * Create an image icon from a file given
	 * @param path An absolute path to the specified file
	 * @param description A quick description of the image file
	 * @return ImageIcon object that can be set to a JLabel in the preview main window
	 */
	protected ImageIcon createImageIcon(String path, String description) {
		File img = new File(path);
		BufferedImage bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(img);
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		if (bufferedImage != null) {
			float h = bufferedImage.getHeight();
			float w = bufferedImage.getWidth();
			float ratio = w / h;
			
			if(h > w) {
				if(h > 500) {
					h = 500;
					w = h * ratio;
				}
			} else {
				if(w > 500) {
					w = 500;
					h = w / ratio;
				}
			}
			
			return new ImageIcon(bufferedImage.getScaledInstance((int)w, (int)h, BufferedImage.SCALE_DEFAULT));
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
    }
	
	/**
	 * Initializer opens search window for format file and sortingFolder
	 */
	void initialize() {
		readFormats();
		File f = openBrowse();
		if(f == null) { f = new File("/C:\\"); }
		folderInput.setText(f.getAbsolutePath());
		sortingFolder = f;
	}
	/**
	 * Does more setup steps, seperated from initialize so it can be used to refresh the folder views
	 * @return
	 */
	int setup() {
		myRef = this;
		addControls();
		int len = folderContent(stuffHold,sortingFolder,100);
		getImages();
		imageCursor = 0;
		setImage();
		
		return len;
	}
	
	
	/**
	 * Setter for pointers
	 * @param stuff - Reference to the container in the scroll view
	 * @param scrollViewPanel - Reference to the scrollview in the main view window
	 * @param viewWindow - Reference to main view window
	 */
	public void giveRef(Container stuff, JPanel scrollViewPanel, ViewWindow viewWindow) {
		stuffHold = stuff;
		scrollHold = scrollViewPanel;
		viewHold = viewWindow;
	}
	/**
	 * Setter for image related pointers
	 * @param imageCont - Reference to the container holding the image label
	 * @param imageLabel - Reference to the image label
	 */
	public void setImagePointers(Container imageCont, JLabel imageLabel) {
		imageVHold = imageCont;
		imageLHold = imageLabel;
	}

}
