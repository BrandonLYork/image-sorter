Use Case:
Place all of your pictures into a single folder
Add all sub folder categories you need to sort all related images

To Use:

1. Select a file with all the formats you wish to sort
Tested file types are as follows
- gif
- png
- jpg
- jpeg
- JPG
- PNG
- BMP
- bmp

A text file is included for formating reference.

2. Make sure your image files are in the folder you wish to sort.
Select this folder as your second folder select option.

3. Sub-folders will be listed on the right.
Clicking on the relevant folder button will open a dialouge window with the options to open additional sub-folders or to save the image.
The image can be copied with "Save Here"
The image can be moved with "Cut and Save"

